<?php

/**
 * @file
 * uw_homepage_feature_type.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_homepage_feature_type_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_feature-type:admin/structure/taxonomy/homepage_feature_type.
  $menu_links['menu-site-manager-vocabularies_feature-type:admin/structure/taxonomy/homepage_feature_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/homepage_feature_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Feature type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_feature-type:admin/structure/taxonomy/homepage_feature_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Feature type');

  return $menu_links;
}
